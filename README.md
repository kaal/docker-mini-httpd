# docker-mini-httpd

Static Website served with a minimal httpd (6 MB)
- https://lipanski.com/posts/smallest-docker-image-static-website
- https://github.com/lipanski/docker-static-website/blob/master/Dockerfile


## Getting started

Create a folder "my-static-website" with static files (html,css,js,...)

```
podman build . -t static-website

podman run -dt --rm -p8080:3000 static-website
```

## Enter the busybox and start httpd server in the shell
```
---------------------------
docker run -it --rm -p80:3000 busybox:1.35
/ # busybox httpd -f -v -p 3000 -h /home/static/
---------------------------
```
